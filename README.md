<div align="center">
  <picture>
    <source media="(prefers-color-scheme: dark)" srcset="assets/monodops-logo-dark.png">
    <source media="(prefers-color-scheme: light)" srcset="assets/monodops-logo-light.png">
    <img alt="Monodops - Logo" src="assets/monodops-logo-solid-light.png" width="75%">
  </picture>
</div>

# Monodops

*Last updated (2024-04-28 06:06)*


<!-- urls -->
[changeset-conventional-commits@pr29]: https://github.com/iamchathu/changeset-conventional-commits/pull/29
[effect-ts]: https://www.effect.website/
[effect-ts@git]: https://github.com/effect-ts/effect-ts/
[mbr-demo@lab]: https://gitlab.com/mblackrittr/mbr-demo/
[mbr-demo@hub]: https://github.com/mblackrittr/mbr-demo/
[mbr-nuxonic@lab]: https://gitlab.com/mblackrittr/mbr-demo/tree/master/apps/nuxt-ionic/
[mbr-nuxonic@hub]: https://github.com/mblackrittr/mbr-demo/tree/master/apps/nuxt-ionic/
[surrealdb]: https://surrealdb.com/
[surrealdb-wasm]: https://github.com/surrealdb/surrealdb.wasm/
